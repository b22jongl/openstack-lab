+++
title = "Setup access"
weight = 2
+++

In this last part, the teacher has setup an OpenStack in a datacenter
(here, on top of Grid'5000) and created regular member account and project
for each of you.  You are no longer admin on OpenStack!

As a preamble, you should connect to the Grid'5000 VPN and then do the
following.  You can do it on your own machine, or from a Grid'5000 frontend
(`ssh <user>@access.grid5000.fr` and then `ssh nantes`).

- [Install the OpenStack CLI](https://github.com/openstack/python-openstackclient/tree/stable/wallaby#getting-started) and [Heat CLI](https://github.com/openstack/python-heatclient/tree/stable/wallaby).
  Here is the process for Grid'5000.
  You may omit the 3 first lines if you do it on your own machine.

```bash
$ pip3 install --upgrade --user pip
$ echo "export PATH=~/.local/bin:${PATH}" >> ~/.profile
$ source ~/.profile
$ pip install --user python-openstackclient python-heatclient
```

- Download the source files for this lab (see [initial setup from the
  previous part](@/openstack-admin/setup.md#lab-resources)).
- Go on the Horizon dashboard of your teacher's OpenStack (the IP address
  should be on the shared pad) and download the "OpenStack RC File" (see
  [previous part](@/openstack-admin/cli-create-vm.md)) on your own
  machine.
  + user name: your Grid'5000 login
  + password: `lab-os`
- Source the "OpenStack RC File".
- Create your admin SSH key (see [previous part about SSH
  keys](@/openstack-admin/ssh-keys.md)).

Resource names change a bit from previously.  Do not hesitate to run
some commands such as the following to know about the new names.

```bash
openstack network list
openstack image list
openstack flavor list
```
