+++
title = "Advanced Heat"
weight = 4
+++

## Data dependency between resources {#data-deps}

Let's declare a template with two VMs: `user` and `provider`.  The
idea is to configure `user`'s static lookup table for hostnames (more
information can be found by typing: `man hosts`), so that user can
target `provider` from its hostname rather than its IP address.  To
that end, the template uses the `user_data` property together with the
`get_attr` function to edit the `/etc/hosts` file on `user`, and map
the IP address of `provider` with its hostname.

```yaml
# 6-boot-vms-with-exchange.yaml
heat_template_version: 2017-09-01

description: >
  Boot two VMs and ease the access from user to provider!

resources:
  user-vm:
    type: "OS::Nova::Server"
    properties:
      name: user
      image: debian-10
      flavor: m1.mini
      key_name: admin
      networks:
        - { network: private }
      user_data:
        str_replace:
          template: |
            #!/usr/bin/env bash
            # With the following line, provider is reachable from its hostname
            echo "${IP_ADDRESS} provider" >> /etc/hosts
          params:
            # `get_attr` references the following `provider-vm` resource.
            ${IP_ADDRESS}: { get_attr: [provider-vm, addresses, private, 0, addr] }

  provider-vm:
    type: "OS::Nova::Server"
    properties:
      name: provider
      image: debian-10
      flavor: m1.mini
      key_name: admin
      networks:
        - { network: private }
```

In this example, `user` requires the IP address of `provider` to boot.
The Heat engine is in charge of managing dependencies between
resources.  Take a look during the deployment, and check that
`provider` is deployed prior to `user`.

```bash
openstack stack create -t ./openstack-user-heat/6-boot-vms-with-exchange.yaml hw6 \
  && watch openstack server list
openstack server add floating ip user \
  $(openstack floating ip create -c floating_ip_address -f value public)
openstack server ssh --login debian --identity ./admin.pem --address-type public user
debian@user:~$ ping provider -c 2
PING provider (192.168.222.238) 56(84) bytes of data.
64 bytes from provider (192.168.222.238): icmp_seq=1 ttl=64 time=1.27 ms
64 bytes from provider (192.168.222.238): icmp_seq=2 ttl=64 time=3.07 ms

debian@user:~$ exit
openstack stack delete --wait --yes hw6
```

## Nested templates

Heat is able to compose templates to keep human-readable files, using
nested templates.  For instance, we can use a first template that
describes a virtual machine, and a second template which deploys multiple
VMs by referencing the first one.  Rather than creating the first
template, we can re-use [a template from previous
examples](@/openstack-user-heat/discovering-heat.md#heat-params).

```yaml
# 7-nested-templates.yaml
heat_template_version: 2017-09-01

description: >
  Boot two different VMs by exploiting nested templates!

resources:
  provider-vm:
    # Template can be provided as resource type (relatively to
    # that template)
    type: ./2_boot_vm_with_params.yaml
    # The related properties are given as template's parameters:
    properties:
      the_vm_name: provider
      the_flavor: m1.mini

  user-vm:
    type: ./2_boot_vm_with_params.yaml
    properties:
      the_vm_name: user
```

To compose a template, a new resource can be defined by specifying its
type as the target of the desired template.  A set of properties can
be provided to the nested template and will be interpreted as
parameters.

```bash
openstack stack create -t ./openstack-user-heat/7-nested-templates.yaml hw7 \
  && watch openstack server list
openstack stack delete --wait --yes hw7
```

Nested templates are very convenient to keep your code clean and
re-usable.  Next section extends nested templates with data
dependency.

## Nested templates with data dependency

Let's describe the [same deployment as before](#data-deps) by using nested
templates. For that we need a new template:

```yaml
# 8-nested-templates-boot-vm.yaml
heat_template_version: 2017-09-01

description: >
  Boot a VM, ease access to a remote host and return its IP address!

parameters:
  the_vm_name:
    type: string
    description: Name of the server
  the_remote_hostname:
    type: string
    description: Host name of the remote host
    default: provider
  the_remote_ip:
    type: string
    description: IP address of the remote host

resources:
  hostname-vm:
    type: "OS::Nova::Server"
    properties:
      name:     { get_param: the_vm_name }
      image:    debian-10
      flavor:   m1.mini
      key_name: admin
      networks:
        - {network: private}
      user_data:
        str_replace:
          params:
            ${HOSTNAME}: { get_param: the_remote_hostname }
            ${IP_ADDRESS}: { get_param: the_remote_ip }
          template: |
            #!/usr/bin/env bash
            # With the following line, the remote host is reachable from its hostname
            echo "${IP_ADDRESS} ${HOSTNAME}" >> /etc/hosts

outputs:
  HOSTIP:
    description: IP address of the created instance
    value: { get_attr: [hostname-vm, networks, private, 0] }
```

We can now declare the main template. While it defines three VMs, this
template is easy to read since it points to the template created just
above, as well as [a template from a previous
example](@/openstack-user-heat/discovering-heat.md#heat-outputs).

```yaml
# 8-nested-templates-exchange.yaml
heat_template_version: 2017-09-01

description: >
  Boot three VMs and ease the access to provider using nested
  templates!

resources:
  provider-vm:
    type: ./3-boot-vm-with-output.yaml

  user-vm1:
    type: ./8-nested-templates-boot-vm.yaml
    properties:
      the_vm_name: user1
      the_remote_ip: { get_attr: [provider-vm, HOSTIP] }
      the_remote_hostname: { get_attr: [provider-vm, HOSTNAME] }

  user-vm2:
    type: ./8-nested-templates-boot-vm.yaml
    properties:
      the_vm_name: user2
      the_remote_ip: { get_attr: [provider-vm, HOSTIP] }
      the_remote_hostname: { get_attr: [provider-vm, HOSTNAME] }
```

```bash
openstack stack create -t ./openstack-user-heat/8-nested-templates-exchange.yaml hw8 \
  && watch openstack server list
openstack stack delete --wait --yes hw8
```

## Other type of resources: floating IP

It's Floating IP time!

```yaml
# 9-floating-ip.yaml
heat_template_version: 2017-09-01

description: >
  Boot a VM and associate a floating IP.

resources:
  server:
    type: OS::Nova::Server
    properties:
      name: hello_fip
      image: debian-10
      flavor: m1.mini
      key_name: admin
      networks:
        - { network: private }

  floating-ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: public

  association:
    type: OS::Neutron::FloatingIPAssociation
    properties:
      floatingip_id: { get_resource: floating-ip }
      port_id: { get_attr: [server, addresses, private, 0, port]}
```

```bash
openstack stack create -t ./openstack-user-heat/9-floating-ip.yaml --wait hw9
```

You may find the floating IP by listing servers.

```bash
openstack server list
```

Or by asking Heat about attributes of the `floating-ip` resource.

```bash
FIP_RSC_ATTRIBUTES=$(openstack stack resource show -c attributes -f value hw9 floating-ip)
python -c "print('floating ip is %s' % ${FIP_RSC_ATTRIBUTES}['floating_ip_address'])"
```

Remember to delete your stack at the end to release resources.

```
openstack stack delete --wait --yes hw9
```
