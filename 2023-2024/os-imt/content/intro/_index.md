+++
title = "Introduction to OpenStack"
weight = 1
sort_by = "weight"
insert_anchor_links = "right"
+++

OpenStack has become the de-facto solution to operate compute, network
and storage resources in public and private clouds.

In this lab, we are going to:
- Deploy an all-in-one OpenStack with [Snap microstack](https://opendev.org/x/microstack/).
- Operate this OpenStack to manage IaaS resources (e.g., boot VMs,
  setup a private Network).
- Deploys a Wordpress as a Service.
- Automatize all the stuff with the Heat template engine (i.e., manage
  your cloud from your sofa!).


## Lecture slides

You can find the slides from the lecture here: 
- [Part1](../openstack-Nov23-part1.pdf) - [Part2](../openstack-Nov23-part2.pdf)

## Exam

Examen papier
Lundi 27/11 de 11H à 12H
