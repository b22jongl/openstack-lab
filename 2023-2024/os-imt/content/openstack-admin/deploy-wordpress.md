+++
title = "Deploy Wordpress as a service"
weight = 7
+++

In the previous sessions, we saw how to boot a VM with OpenStack, and
execute a post-installation script using the `user-data` mechanism.
Such mechanism can help us to install software but it is not enough to
deploy a real Cloud application.  Cloud applications are composed of
multiple services that collaborate to deliver the application.  Each
service is in charge of one aspect of the application.  This
separation of concerns brings flexibility.  If a single service is
overloaded, it is common to deploy new units of this service to
balance the load.

Let's take a simple example: [WordPress](https://wordpress.org/)!
WordPress is a very popular content management system (CMS) in use on the
Web.  People use it to create websites, blogs or applications.  It is
open-source, written in PHP, and composed of two elements: a Web server
(Apache) and database (MariaDB).  Apache serves the PHP code of WordPress
and stores its information in the database.

Automation is a very important concept for DevOps.  Imagine you have
your own datacenter and want to exploit it by renting WordPress
instances to your customers.  Each time a client rents an instance,
you have to manually deploy it?!  No. It would be more convenient to
automate all the operations. 😎

{% do() %}
As the DevOps of OWPH -- Online WordPress Hosting -- your job is to
automatize the deployment of WordPress on your OpenStack.  To do so,
you have to make a bash script that:

1. Start `wordpress-db`: a VM that contains the MariaDB database for
   WordPress.
2. Wait until its final deployment (the database is running).
3. Start `wordpress-app`: a VM that contains a web server and serves
   the Wordpress CMS.
4. Expose `wordpress-app` to the world via your Lab machine on a
   specific port (because our floating IPs are not real public IPs and
   thus inaccessible from the world). Something like
   `http://<ip-of-your-lab>:8080`.
5. Finally, connect with your browser to the WordPress website (i.e.,
   `http://<ip-of-your-lab>:8080/wp`) and initializes a new WordPress
   project named `os-owph`.

The `openstack-admin` directory provides bash scripts to deploy the
MariaDB database and web server of WordPress.  Review it before going any
further (spot the **{{ TODO }}**).  And ask yourself questions such as:
Does the `wordpress-db` VM needs a floating IP in order to be reached by
the `wordpress-app` VM?

Also, remember to first clean up your old VMs:

```bash
# Delete VMs
for vm in $(openstack server list -c ID -f value); do \
  echo "Deleting ${vm}..."; \
  openstack server delete "${vm}"; \
done

# Releasing floating IPs
for ip in $(openstack floating ip list -c "Floating IP Address" -f value); do \
  echo "Releasing ${ip}..."; \
  openstack floating ip delete "${ip}"; \
done
```
{% end %}

{% solution() %}
Find the solution in the `openstack-admin/wordpress-deploy.sh` script of the
tarball.

First thing first, enable HTTP connections.
```bash
SECGROUP_ID=$(openstack security group list --project admin -f value -c ID)
openstack security group rule create $SECGROUP_ID \
  --proto tcp --remote-ip 0.0.0.0/0 \
  --dst-port 80
```

Then start a VM with the `wordpress-db` name, `debian-10` image,
`m1.mini` flavor, `test` network and `admin` key-pair. Also,
contextualize your VM with the `openstack-admin/install-mariadb.sh` script thanks
to the `--user-data ./openstack-admin/install-mariadb.sh` option.

```bash
openstack server create --wait --image debian-10 \
  --flavor m1.mini --network test \
  --key-name admin \
  --user-data ./openstack-admin/install-mariadb.sh \
  wordpress-db

wait_contextualization wordpress-db
```

Next, start a VM with `wordpress-app` name, `debian-10` image,
`m1.mini` flavor, `test` network and `admin` key-pair. Also,
contextualize your VM with the `openstack-admin/install-wp.sh` script thanks to
the `--user-data ./openstack-admin/install-wp.sh` option. Note that you need to
provide the IP address of the `wordpress-db` to this script before
running it.

Set the script with IP address of `wordpress-db`.
```bash
sed -i '13s|.*|DB_HOST="'$(openstack server show wordpress-db -c addresses -f value | sed -Er "s/test=//g")'"|' ./openstack-admin/install-wp.sh
```

Then, create `wordpress-app`.
```bash
openstack server create --wait --image debian-10 \
  --flavor m1.mini --network test \
  --key-name admin \
  --user-data ./openstack-admin/install-wp.sh \
  wordpress-app

wait_contextualization wordpress-app
```

Get a floating ip for the VM.
```bash
WP_APP_FIP=$(openstack floating ip create -c floating_ip_address -f value external)
```

Attach the `WP_APP_FIP` floating ip to that VM.
```bash
openstack server add floating ip wordpress-app "${WP_APP_FIP}"
```

Setup redirection to access your floating ip on port 80:
```bash
sudo iptables -t nat -A PREROUTING -p tcp --dport 8080 \
    -j DNAT --to "${WP_APP_FIP}:80"
```

Finally, you can reach WordPress on `http://<ip-of-your-lab>:8080/wp`

Optionally, you can get access with an SSH tunnel to access `10.20.20.*`
from your own machine:

```bash
ssh -NL 8080:<floating-ip>:80 -l root <ip-of-your-lab-machine>
```

And reach WordPress on <http://localhost:8080/wp>.
{% end %}

Once your bash script completes, you should be able to access WordPress at
`http://<ip-of-your-lab>:8080/wp`.
